﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {

	[System.Serializable]
	public class LevelDetails{
		
		
		public string LevelID;
		public int targetGoal;
		public bool isbossLvl;
	
	}

	[SerializeField]
	private tk2dUIItem[] lvlButton;
	[SerializeField]
	private tk2dUIItem btnLeague; 
	[SerializeField]
	private tk2dUIItem btnArcade;


	//tk2dTextMesh
	public TextMesh txtLevel;
	public TextMesh txtGoal;
	public TextMesh txtBest;

	
	public TextMesh txtArcadeBest;

	
	public tk2dUIItem touchBlock_LvlDetail; 

	[SerializeField]
	private LevelDetails[] lvlDetails;


	Transform levelDetailsWindow;
	bool confirmIsFocus = false;

	int selectedLevel = 0;

	void Start () {

		levelDetailsWindow = GameObject.Find("Panel - Level Details").transform;
		lvlButton = new tk2dUIItem[this.transform.childCount];
		for(int x = 0; x < this.transform.childCount-1; x++){

			if(lvlButton[x] == null && lvlDetails[x] !=null){
				lvlButton[x] = this.transform.GetChild(x).GetComponent<tk2dUIItem>();

				//button[x].OnClick += () => LoadLevelDetails(lvlDetails[x]);

			
			}
		}
		if(("ArcadeBest") == null){
			PlayerPrefs.SetInt("ArcadeBest",0);
		}

		txtArcadeBest.text = PlayerPrefs.GetInt("ArcadeBest") + " Points";

		lvlButton[0].OnClick += () => SetLevelDetails(lvlDetails[0]);
		lvlButton[1].OnClick += () => SetLevelDetails(lvlDetails[1]);
		lvlButton[2].OnClick += () => SetLevelDetails(lvlDetails[2]);
		
		btnLeague.OnClick += LoadGame;
		btnArcade.OnClick += () => LoadGame(true);
		touchBlock_LvlDetail.OnClick += unloadLevelDetails;
	}

	void LoadGame(bool isArcade = false){

		if(!isArcade){
			if(GameObject.Find("Game Info")==null){
				GameObject levelInfo = new GameObject();
				levelInfo.name = "Game Info";
				levelInfo.AddComponent<CurLevelInfo>();
			}
			CurLevelInfo lvInfo = GameObject.Find("Game Info").GetComponent<CurLevelInfo>();

			lvInfo.TargetScore = lvlDetails[selectedLevel].targetGoal;
			lvInfo.isBossBattle = lvlDetails[selectedLevel].isbossLvl;
		}

		Application.LoadLevel("_Game");

	}

	// Update is called once per frame
	void SetLevelDetails(LevelDetails level) {

		//slide in from the right
		if(!confirmIsFocus){
			confirmIsFocus = true;
			levelDetailsWindow.position = new Vector3(3,0,-5);
		}

		txtLevel.text = level.LevelID;
		if(!level.isbossLvl){
			txtGoal.text = "Goal: "+level.targetGoal+" Points";
		}
		else{
			txtGoal.text = "Beat the Boss!";
		}
		
		if(PlayerPrefs.GetInt("L1BestScore") == null){
			PlayerPrefs.SetInt("L1BestScore",0);
		}
		txtBest.text = "Best: "+PlayerPrefs.GetInt("L1BestScore")+" Points";
		selectedLevel = int.Parse(level.LevelID.Substring(6)) - 1;
	}


	void unloadLevelDetails(){
		confirmIsFocus = false;
		//slide away from the right
		levelDetailsWindow.position = new Vector3(30,0,0);

	}
}
