using UnityEngine;
using System.Collections;

public class GameManager_FT : MonoBehaviour {
	
	public enum GameState {  
		Start,
		Playing,
		GameOver,
		Restart,
		Pause,
	}

	public GameState gameState = GameState.Start;
	
	public GameObject combo_image;
	//public GameObject combo_Count;
	public GameObject combo_Number1;
	public GameObject combo_Number10;
	public GameObject combo_Number100;
	
	public GameObject ComboGObj;

	public GameObject combo_plus;
	
	public GameObject bg;
	public GameObject bg_super1;
	public GameObject bg_super2;
	public GameObject bg_super3;
	public GameObject bg_super4;
	
	public GameObject timebar_wide;
	public GameObject timebar_widegauge;
	public GameObject timebar_slow;
	public GameObject timebar_slowgauge;
	public GameObject timebar_super;
	public GameObject timebar_supergauge;
	
	public tk2dUIItem retry;
	public tk2dUIItem returnMap;
	public tk2dUIItem unPause;

	public tk2dUIItem MuteSound;
	
	public tk2dUIItem fbShare;


	public tk2dSprite stars;

	public TextMesh GameResult;
	public TextMesh MedalsCollected;
	//public tk2dTextMesh GameResult;
	//public tk2dTextMesh MedalsCollected;


	tk2dSpriteAnimator[] pScoreSprt;

	
	
	void OnEnable()
    {

        retry.OnClick += RestartGame;
    }

    void OnDisable()
    {
        retry.OnClick -= RestartGame;
    }
	
		
	void Start () {


		
		pScoreSprt =  new tk2dSpriteAnimator[5];
		
		pScoreSprt[0] = GameObject.Find("PScore_Number1").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[1] = GameObject.Find("PScore_Number10").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[2] = GameObject.Find("PScore_Number100").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[3] = GameObject.Find("PScore_Number1000").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[4] = GameObject.Find("PScore_Number10000").GetComponent<tk2dSpriteAnimator>();

		Global_FT.time_game = 120f;
		
		Time.timeScale = 1.0f;
		iTween.ScaleTo( ComboGObj, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 0f, "easetype", "linear" ) );
		iTween.MoveTo( ComboGObj, iTween.Hash("x", 25.0f, "y", 3.0f, "z", -20.0f, "time", 0.1f, "delay", 0f, "easetype", "linear" ) );
		//iTween.ScaleTo( combo_image, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 0f, "easetype", "linear" ) );
		//iTween.ScaleTo( combo_Count, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 0f, "easetype", "linear" ) );
		
		//iTween.ScaleTo( combo_Number1, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 0f, "easetype", "linear" ) );
		//iTween.ScaleTo( combo_Number10, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 0f, "easetype", "linear" ) );
		//iTween.ScaleTo( combo_Number100, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 0f, "easetype", "linear" ) );
		//iTween.ScaleTo( combo_plus, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 0f, "easetype", "linear" ) );
		
		
	//	RestartGame ();
		
	}
	
	public void RestartGame ()
	{
		
		Global_FT.PlayerScore = 0;
		Global_FT.time_game = 120f;
		Global_FT.boom_count = 0;
		
		gameState = GameState.Playing;	
		
		GameObject.Find("GameOver").transform.position = new Vector3(0, 300, 0);
		
		Time.timeScale = 1;



		//alternate
		//reload level/
	}
	
	
	
	// Update is called once per frame
	void Update () {
		
		//if( gameState != GameState.Playing )
		//	return;
		AdjustDifficulty ();
		
		Global_FT.time_game -= Time.deltaTime;
		
		if( Global_FT.time_game < 0 )
		{
			Global_FT.time_game = 0;
			gameState = GameState.GameOver;	
			
			Time.timeScale = 0;
			
			GameObject.Find("GameOver").transform.position = new Vector3(0, 0, 0);
			
		}
		
		if( Global_FT.bWideState == true )
		{
			Global_FT.time_wide -= Time.deltaTime;
			if( Global_FT.time_wide  < 0 )
			{
				Global_FT.time_wide = -1;
				
				Global_FT.bWideState = false;
				
				GameObject[] objlist;
			   	objlist = GameObject.FindGameObjectsWithTag("Rim");
				for( int i=0; i<objlist.Length; i++ )
				{
					objlist[i].SendMessage( "OnBecomeNormal", SendMessageOptions.DontRequireReceiver );
				}
				
				timebar_wide.SetActive(false);	
				timebar_widegauge.SetActive(false);	
				timebar_widegauge.transform.localScale = new Vector3( 10, 10, 1 );
			}
			else
			{
				if( timebar_wide.active == false )
				{
					timebar_wide.SetActive(true);	
					timebar_widegauge.SetActive(true);	
				}
				
				timebar_widegauge.transform.localScale = new Vector3( Global_FT.time_wide / 10f * 10, 10, 1 );
			}
		}
		
		if( Global_FT.bSlowState == true )
		{
			Global_FT.time_slow -= Time.deltaTime;
			if( Global_FT.time_slow  < 0 )
			{
				Global_FT.time_slow = -1;
				
				Global_FT.bSlowState = false;
				
				GameObject.Find("BGM").GetComponent<BGM_FT>().Stop_BGM();
				GameObject.Find("BGM").GetComponent<BGM_FT>().Start_BGM();
				

				timebar_slow.SetActive(false);	
				timebar_slowgauge.SetActive(false);	
				timebar_slowgauge.transform.localScale = new Vector3( 10, 10, 1 );
			}
			else
			{
				if( timebar_slow.active == false )
				{
					timebar_slow.SetActive(true);	
					timebar_slowgauge.SetActive(true);	
				}
				
				timebar_slowgauge.transform.localScale = new Vector3( Global_FT.time_slow / 10f * 10, 10, 1 );
			}
		}
		
		if( Global_FT.bSuperState == true )
		{
			
			Global_FT.time_superbg -= Time.deltaTime;
			if( Global_FT.time_superbg  < 0 )
			{
				Global_FT.time_superbg = 0.1f;
				
				Global_FT.index_superbg++;
				if( Global_FT.index_superbg > 3)
					Global_FT.index_superbg = 0;
				
				this.bg.SetActive(false);
				this.bg_super1.SetActive(false);
				this.bg_super2.SetActive(false);
				this.bg_super3.SetActive(false);
				this.bg_super4.SetActive(false);
				
				if( Global_FT.index_superbg == 0 )
					this.bg_super1.SetActive(true);
				else if( Global_FT.index_superbg == 1 )
					this.bg_super2.SetActive(true);
				else if( Global_FT.index_superbg == 2 )
					this.bg_super3.SetActive(true);
				else if( Global_FT.index_superbg == 3 )
					this.bg_super4.SetActive(true);
			}

			Global_FT.time_super -= Time.deltaTime;
			if( Global_FT.time_super  < 0 )
			{
				Global_FT.time_super = -1;
				
				Global_FT.bSuperState = false;
				
				this.bg.SetActive(true);
				this.bg_super1.SetActive(true);
				this.bg_super2.SetActive(true);
				this.bg_super3.SetActive(true);
				this.bg_super4.SetActive(true);
				
				timebar_super.SetActive(false);	
				timebar_supergauge.SetActive(false);	
				timebar_supergauge.transform.localScale = new Vector3( 10, 10, 1 );
			
			}
			else
			{
			
				if( timebar_super.active == false )
				{
					timebar_super.SetActive(true);	
					timebar_supergauge.SetActive(true);	
				}
				
				timebar_supergauge.transform.localScale = new Vector3( Global_FT.time_super , 10, 1 );
			}
		}
		
		if( Application.loadedLevelName == "_game" )
		{
			
			if( gameState == GameState.Playing )
			{
				
			}
		}
	}

	public void AdjustDifficulty(){
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			Global_FT.gameDif = Global_FT.Difficulty.Easy;
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2)) {
			Global_FT.gameDif = Global_FT.Difficulty.Norm;
		} 

		else if (Input.GetKeyDown(KeyCode.Alpha3)) {
			Global_FT.gameDif = Global_FT.Difficulty.Hard;
		} 
	}
	
	
	public void DispCombo ( int combo_count ) {
	

		Global_FT.PlayerScore += ( combo_count - 1) * 400;
		
		int number_1 = Mathf.Clamp(Global_FT.PlayerScore % 10, 0, 9);
		int number_10 = Mathf.Clamp(Global_FT.PlayerScore / 10 % 10, 0, 9);
		int number_100 = Mathf.Clamp(Global_FT.PlayerScore / 100 % 10, 0, 9);
		int number_1000 = Mathf.Clamp(Global_FT.PlayerScore / 1000 % 10, 0, 9);
		int number_10000 = Mathf.Clamp(Global_FT.PlayerScore / 10000 % 10, 0, 9);

			
		pScoreSprt[0].Play( number_1.ToString() );
		pScoreSprt[1].Play( number_10.ToString() );
		pScoreSprt[2].Play( number_100.ToString() );
		pScoreSprt[3].Play( number_1000.ToString() );
		pScoreSprt[4].Play( number_10000.ToString() );

		/*
		iTween.ScaleTo( combo_image, iTween.Hash("x", 10.0f, "y", 10.0f, "z", 1.0f, "time", 0.1f, "delay", 0.0f, "easetype", "linear" ) );
		iTween.ScaleTo( combo_image, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 1.0f, "easetype", "linear" ) );
	
		//combo_Count.GetComponent<tk2dSpriteAnimator>().Play( combo_count.ToString() );
		//iTween.ScaleTo( combo_Count, iTween.Hash("x", 10.0f, "y", 10.0f, "z", 1.0f, "time", 0.1f, "delay", 0.0f, "easetype", "linear" ) );
		//iTween.ScaleTo( combo_Count, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 1.0f, "easetype", "linear" ) );
		

		
		iTween.ScaleTo( combo_Number1, iTween.Hash("x", 10.0f, "y", 10.0f, "z", 1.0f, "time", 0.1f, "delay", 0.0f, "easetype", "linear" ) );
		iTween.ScaleTo( combo_Number1, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 1.0f, "easetype", "linear" ) );
		
		iTween.ScaleTo( combo_Number10, iTween.Hash("x", 10.0f, "y", 10.0f, "z", 1.0f, "time", 0.1f, "delay", 0.0f, "easetype", "linear" ) );
		iTween.ScaleTo( combo_Number10, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 1.0f, "easetype", "linear" ) );
		
		iTween.ScaleTo( combo_Number100, iTween.Hash("x", 10.0f, "y", 10.0f, "z", 1.0f, "time", 0.1f, "delay", 0.0f, "easetype", "linear" ) );
		iTween.ScaleTo( combo_Number100, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 1.0f, "easetype", "linear" ) );
		
		iTween.ScaleTo( combo_plus, iTween.Hash("x", 10.0f, "y", 10.0f, "z", 1.0f, "time", 0.1f, "delay", 0.0f, "easetype", "linear" ) );
		iTween.ScaleTo( combo_plus, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 1.0f, "easetype", "linear" ) );
		*/

		number_1 = Mathf.Clamp(combo_count*100 % 10, 0, 9);
		number_10 = Mathf.Clamp(combo_count*100 / 10 % 10, 0, 9);
		number_100 = Mathf.Clamp(combo_count*100 / 100 % 10, 0, 9);

		
		
		combo_Number1.GetComponent<tk2dSpriteAnimator>().Play( number_1.ToString() );
		combo_Number10.GetComponent<tk2dSpriteAnimator>().Play( number_10.ToString() );
		combo_Number100.GetComponent<tk2dSpriteAnimator>().Play( number_100.ToString() );

		iTween.ScaleTo(ComboGObj, iTween.Hash("x", 10.0f, "y", 10.0f, "z", 1.0f, "time", 0.1f, "delay", 0.0f, "easetype", "linear" ) );
		iTween.ScaleTo(ComboGObj, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 1.0f, "easetype", "linear" ) );
		
	}
	
	

	//Start
	
	public void OnBtn_Start_GameStart() {
	
		Debug.Log( "OnBtn_Start_GameStart" );
		
		GameObject.Find("UI_Start").transform.position = new Vector3(0, 0, 0);
		
		gameState = GameState.Playing;
		
//		Owner.enabled = true;
	}
	
	public	IEnumerator waitforsec_start ( float delay )
	{
		yield return new WaitForSeconds(delay);	
		
	}
	
	public void OnBtn_Game_Pause() {
	
		Debug.Log( "OnBtn_Game_Pause" );
		
		GameObject.Find("Pause").transform.position = new Vector3(0, 0, -40f);
		
		gameState = GameState.Pause;
		
		Time.timeScale = 0.0f;
		
	}
	
	public void OnBtn_Pause_Restart() {
	
		Debug.Log( "OnBtn_Pause_Restart" );
		
		Application.LoadLevelAsync ("_game" );
	}
	
	public void OnBtn_Pause_Quit() {
	
		Debug.Log( "OnBtn_Pause_Quit" );
		
		Application.LoadLevelAsync ("_mainmenu" );
		
		
	}
	
	public void OnBtn_Pause_Back() {
	
		Debug.Log( "OnBtn_Pause_Back" );
		
		gameState = GameState.Playing;
		
		Time.timeScale = 1.0f;
		
		GameObject.Find("Pause").transform.position = new Vector3(-430f, 0, -40f);
	}
	
	public void OnBtn_Pause_BGM() {
	
		Debug.Log( "OnBtn_Pause_BGM" );
		
	}
	
	public void OnBtn_Pause_Help() {
	
		Debug.Log( "OnBtn_Pause_BGM" );
		
	}
	
	public void OnBtn_Pause_Sound() {
	
		Debug.Log( "OnBtn_Pause_Sound" );
		
	}
}
