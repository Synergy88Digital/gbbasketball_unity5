using UnityEngine;
using System.Collections;

public class TimeBar : MonoBehaviour {


	tk2dClippedSprite Timebar;

	void Start(){
		Timebar = transform.GetComponent<tk2dClippedSprite> ();
		}

	// Update is called once per framea
	void Update () {

		//Without rescaling the entire sprite

		Timebar.ClipRect = new Rect(0,0,Global_FT.time_game/120f, 1);


		//Rescales entire sprite
		//transform.localScale = new Vector3( Global_FT.time_game / 120f * 10, 10, 1 );
			
	}
}
