using UnityEngine;
using System.Collections;

public class BoomBar : MonoBehaviour {


	
	tk2dClippedSprite boomBar;

	void Start(){
		boomBar = transform.GetComponent<tk2dClippedSprite> ();
	}
	
	// Update is called once per framea
	void Update () {
		
		//Without rescaling the entire sprite
		
		boomBar.ClipRect = new Rect(0,0,Global_FT.boom_count/100f, 1);

		//transform.localScale = new Vector3( Global_FT.boom_count / 100f * 10, 10, 1 );
			
	}
}
