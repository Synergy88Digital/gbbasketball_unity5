﻿using UnityEngine;
using System.Collections;

public class EnemyAI : Character_FT {


	
	public float rateOfFire = 1f;
	private float shootTime = 0;	

	private int target = 1;

	public GameObject BoardTop;
	public GameObject BoardMid;
	public GameObject BoardBot;

	private float xPosOffset = 0;

	
	//private bool hasShooted = false;

	private float prevTranslation = 0;

	private GameObject targetHoop = null;

	private bool isRapidFire = false;

	private Global_FT.Difficulty curDifficulty = Global_FT.Difficulty.Easy;

	void Start(){
		if (BoardTop == null)
			BoardTop = GameObject.Find ("Board_Line1_Top");
		if(BoardMid == null)
			BoardMid = GameObject.Find ("Board_Line2_Middle");
		if(BoardBot == null)
			BoardBot = GameObject.Find ("Board_Line3_Bottom");
	}
	void Update () {
		AIBehaviour ();
		//Time.timeScale = 2;
	}

	void LateUpdate(){

		//for Testing/Mockup only
		if (curDifficulty != Global_FT.gameDif) {
			if (Global_FT.gameDif == Global_FT.Difficulty.Easy) {
				rateOfFire = 1.30f;

			} else if (Global_FT.gameDif == Global_FT.Difficulty.Norm) {
				rateOfFire = .8f;

			} else if (Global_FT.gameDif == Global_FT.Difficulty.Hard) {
				rateOfFire = 0.5f;
			}
			curDifficulty = Global_FT.gameDif;
		}
	}


	void TargetSelect(){
		
		if(targetHoop == null){
			
			int probability = Random.Range(0,10);
			if(curDifficulty == Global_FT.Difficulty.Easy){
				
				if(probability==0)
					target =  3;
				else if(probability<=3)
					target =  2;
				else
					target =  1;
				
			}
			else if (curDifficulty == Global_FT.Difficulty.Norm){
				
				if(probability<=1)
					target =  3;
				else if(probability<=6)
					target =  2;
				else
					target =  1;
				
			}
			else if (curDifficulty == Global_FT.Difficulty.Hard){
				
				if(probability<=3)
					target =  2;
				else
					target =  3;
			}


			if (target == 3) {
				targetHoop = BoardTop.transform.GetChild(Random.Range(0,BoardTop.transform.childCount)).gameObject;

			}
			else if(target == 2){
				targetHoop = BoardMid.transform.GetChild(Random.Range(0,BoardTop.transform.childCount)).gameObject;   

			}
			else if(target == 1){
				targetHoop = BoardBot.transform.GetChild(Random.Range(0,BoardTop.transform.childCount)).gameObject;

			}
			
		}

		//Power up priority
		if(false) // uncomment to disable feature
		if(curDifficulty == Global_FT.Difficulty.Hard && Global_FT.BoardPowerUps.Count>0){
			for(int x = 0;x<Global_FT.BoardPowerUps.Count;x++){
				if(Global_FT.BoardPowerUps[x].transform.position.x < 90 && Global_FT.BoardPowerUps[x].transform.position.x > -90){
					targetHoop = Global_FT.BoardPowerUps[x];
					
				}
			}
			
		}
		//calculation of x position offset for accuracy
		Board boardRow = targetHoop.GetComponent<Board>();
		xPosOffset = 14.5f;

		if(boardRow.boardIndex == 2){
			xPosOffset *= 1.4f * boardRow.speed_level_bottom;
		}
		else if(boardRow.boardIndex  == 1){
			xPosOffset *= -1 * boardRow.speed_level_mid * 1.4f;
		}
		else if(boardRow.boardIndex  == 0){
			xPosOffset *= boardRow.speed_level_top * 1.4f;
		}
		if(Global_FT.bSlowState == true )
			xPosOffset /= 2.4f;
		//xPosOffset /= 2.4f;
		
	}

	void AIBehaviour(){

		
		if(shootTime>=0)
			shootTime -= Time.deltaTime;

		float translation = 0;

		TargetSelect();

		if( Global_FT.bSuperState == true && !isRapidFire){
			isRapidFire = true;
			rateOfFire /= 2f;
		}
		else if( Global_FT.bSuperState == false && isRapidFire){
			isRapidFire = false;
			rateOfFire *= 2f;
		}
			
			//for Hard Difficulty
		if(targetHoop != null && curDifficulty == Global_FT.Difficulty.Hard ){
			if (Mathf.Abs (this.gameObject.transform.position.x - targetHoop.transform.position.x) > 45) {
				speed = 8;
			}
			else if(Mathf.Abs (this.gameObject.transform.position.x - targetHoop.transform.position.x) <0.5) {
				speed = 3;
			}
		}


		if (animState != AnimState.Shoot && targetHoop!= null) {

			//Determines the direction to which the character will run
			if(this.transform.position.x > targetHoop.transform.position.x + xPosOffset* 1.25f){
				translation = -1 ;
			}
			else{
				translation = 1 ;
			}
		
			//if new direction is established, reset to idle form
			if(prevTranslation != translation){
				AnimProcess(0);
				prevTranslation = translation;
			} 

			//Prevents Character from trying to go outside the screen
			if((targetHoop.transform.position.x + xPosOffset ) >115 || (targetHoop.transform.position.x + xPosOffset * 1.25f)< -120){
				targetHoop = null;
				//translation = 0;
				return;
			}

			//Prevents Twiching Character
			if(false && Mathf.Abs(targetHoop.transform.position.x - (this.transform.position.x - xPosOffset * 1.25f))<2){
				targetHoop = null;
				//translation = 0;
				return;
			}
		
			translation *= Time.deltaTime * speed * 25f;
		
			transform.Translate (translation, 0, 0);
			
			AnimProcess (translation);

			if (shootTime < 0){
			
				shootTime = rateOfFire;

				speed = 3; 

				animState = AnimState.Shoot;
				
				tk2dSprite_char.AnimationCompleted = AnimDone_Shoot;
				tk2dSprite_char.AnimationEventTriggered = null;
				tk2dSprite_char.Play("Shoot");
				
				ThrowBall (targetHoop.transform.position.y+5);		
				targetHoop = null;
			}
		}

	}

}
