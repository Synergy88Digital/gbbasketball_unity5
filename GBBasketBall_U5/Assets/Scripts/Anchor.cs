using UnityEngine;
using System.Collections;

public class Anchor : MonoBehaviour {
	
	public bool LeftBottomAlign = false;
	public bool RightBottomAlign = false;
	public bool LeftTopAlign = false;
	public bool RightTopAlign = false;
	
	public float marginLeft = 0;
	public float marginRight = 0;
	public float marginTop = 0;
	public float marginBottom = 0;
	
	public float defaultWidth = 800;
	public float defaultHeight = 480;
	
	float x, y;
	
	// Use this for initialization
	void Start () {
	

		if( LeftBottomAlign == true )
		{
			x = GetComponent<GUITexture>().pixelInset.x;
			y = GetComponent<GUITexture>().pixelInset.y;
			//x = marginLeft;
			//y = marginBottom;
			GetComponent<GUITexture>().pixelInset = new Rect(x, y, GetComponent<GUITexture>().pixelInset.width, GetComponent<GUITexture>().pixelInset.height);
		}
		else if( RightBottomAlign == true )
		{
			marginRight = defaultWidth - GetComponent<GUITexture>().pixelInset.x - GetComponent<GUITexture>().pixelInset.width;
			x = Screen.width - (int)GetComponent<GUITexture>().pixelInset.width - marginRight;
			y = GetComponent<GUITexture>().pixelInset.y;
			//x = Screen.width - (int)guiTexture.pixelInset.width - marginRight;
			//y = marginBottom;
			GetComponent<GUITexture>().pixelInset = new Rect(x, y, GetComponent<GUITexture>().pixelInset.width, GetComponent<GUITexture>().pixelInset.height);
		}
		else if( LeftTopAlign == true )
		{
			marginTop = defaultHeight - GetComponent<GUITexture>().pixelInset.y - GetComponent<GUITexture>().pixelInset.height;
			x = GetComponent<GUITexture>().pixelInset.x;
			y = Screen.height - (int)GetComponent<GUITexture>().pixelInset.height - marginTop;
			//x = marginLeft;
			//y = Screen.height - (int)guiTexture.pixelInset.height - marginTop;
			GetComponent<GUITexture>().pixelInset = new Rect(x, y, GetComponent<GUITexture>().pixelInset.width, GetComponent<GUITexture>().pixelInset.height);
		}
		else if( RightTopAlign == true )
		{
			marginRight = defaultWidth - GetComponent<GUITexture>().pixelInset.x - GetComponent<GUITexture>().pixelInset.width;
			marginTop = defaultHeight - GetComponent<GUITexture>().pixelInset.y - GetComponent<GUITexture>().pixelInset.height;
			x = Screen.width - (int)GetComponent<GUITexture>().pixelInset.width - marginRight;
			y = Screen.height - (int)GetComponent<GUITexture>().pixelInset.height - marginTop;
			//x = Screen.width - (int)guiTexture.pixelInset.width - marginRight;
			//y = Screen.height - (int)guiTexture.pixelInset.height - marginTop;
			GetComponent<GUITexture>().pixelInset = new Rect(x, y, GetComponent<GUITexture>().pixelInset.width, GetComponent<GUITexture>().pixelInset.height);
		}
		
	}
	
}
