using UnityEngine;
using System.Collections;

public class Character_FT : MonoBehaviour {
	
	public GameObject ball;
	public GameObject mousepoint;
	
	float time_throw = 1f;
	public float speed = 100.0F;
	
	GameObject clone;
	
	public GameObject objSprite;
	public tk2dSpriteAnimator tk2dSprite_char;
	
	public GameObject objCharacter;
	
	public enum AnimState {  
		Idle,
		Run,
		Shoot,
		Turn,
	}

	public AnimState animState = AnimState.Idle;
	
	public tk2dUIItem boombtn;
	
	// Use this for initialization
	void Start () {
	
	}
	
	void OnEnable()
    {
        boombtn.OnClick += Boom;
    }

	void OnDisable()
    {
        boombtn.OnClick -= Boom;
    }
	
	
	// Update is called once per frame
	void Update () {

		if(GameManager_FT.gameState == GameManager_FT.GameState.Playing){
			PlayerControls();
		}
	}

	void PlayerControls(){
		time_throw -= Time.deltaTime;
		
		float translation = 0;
		
		if( Application.isEditor )
		{
			if( Input.GetMouseButtonDown( 0 ) && time_throw < 0)
			{
				
				Vector3 MousePos = GameObject.Find("Main Camera").GetComponent<Camera>().ScreenToWorldPoint( new Vector3( Input.mousePosition.x, Input.mousePosition.y, -30f) );
				//Debug.Log("MousePos : "+MousePos);
				
				//mousepoint.transform.position = new Vector3( MousePos.x, MousePos.y, -30f );
				mousepoint.transform.position = new Vector3( objSprite.transform.position.x, MousePos.y, -30f );
				
				if( Global_FT.bSuperState == true )
					time_throw = 0.2f;
				else
					time_throw = 1f;
				
				animState = AnimState.Shoot;
				
				tk2dSprite_char.AnimationCompleted = AnimDone_Shoot;
				tk2dSprite_char.AnimationEventTriggered = null;
				tk2dSprite_char.Play("Shoot");
				
				
				ThrowBall ( MousePos.y );
			}
			
			if( animState != AnimState.Shoot )
			{
				translation = Input.GetAxis("Horizontal") * speed;
				translation *=  Time.deltaTime * 20f;
				
				transform.Translate( translation , 0, 0);
				
				AnimProcess ( translation );
			}
		}
		else
		{
			int count  = Input.touchCount;	
			Touch touch;
			Vector3 touchPos;
			
			for(int i = 0; i < count; i++)
			{
				touch = Input.GetTouch(0);
				
				if( (touch.phase == TouchPhase.Began || 
				     touch.phase == TouchPhase.Moved ||
				     touch.phase == TouchPhase.Stationary  ) )
				{
					touchPos = new Vector3( touch.position.x, touch.position.y, 0 );	
					//Debug.Log("touch pos : "+touchPos);
					
					if( touchPos.y < 200 )
					{
						
						if( touchPos.x < 480 ) //Left if( touchPos.x < player.transform.position.x ) //
						{
							translation = -1 * speed;
						}
						else
						{
							translation = 1 * speed;
						}
						
					}
					else
					{
						if( (touch.phase == TouchPhase.Began  && time_throw < 0) )
						{
							
							touchPos = new Vector3( touch.position.x, touch.position.y, 0 );
							
							//Debug.Log("touch pos : "+touchPos);
							
							Vector3 MousePos = GameObject.Find("Main Camera").GetComponent<Camera>().ScreenToWorldPoint( new Vector3( touchPos.x, touchPos.y, -30f) );
							//Debug.Log("MousePos : "+MousePos);
							
							//mousepoint.transform.position = new Vector3( MousePos.x, MousePos.y, -30f );
							mousepoint.transform.position = new Vector3( objSprite.transform.position.x, MousePos.y, -30f );
							
							if( Global_FT.bSuperState == true )
								time_throw = 0.2f;
							else
								time_throw = 1f;
							
							animState = AnimState.Shoot;
							
							tk2dSprite_char.AnimationCompleted = AnimDone_Shoot;
							tk2dSprite_char.AnimationEventTriggered = null;
							tk2dSprite_char.Play("Shoot");
							
							ThrowBall ( MousePos.y );
							
						}
					}
					
				}
			}
			
			if( animState != AnimState.Shoot )
			{
				translation *=  Time.deltaTime * 20f;
				
				if( (translation > 0 && transform.position.x < 120) ||
				   (translation < 0 && transform.position.x > -130 )  )
					transform.Translate( translation , 0, 0);
				
				AnimProcess ( translation );
			}
			
		}

	}
	
	protected void AnimProcess ( float translation )
	{
		if( animState == AnimState.Idle )
		{
			if( translation > 0.1f )
			{
				animState = AnimState.Run;
				objSprite.transform.eulerAngles = new Vector3( 0, 180, 0 );
				
				tk2dSprite_char.Play("Run");
				
			}
			else if(  translation < -0.1f )
			{
				animState = AnimState.Run;
				objSprite.transform.eulerAngles = new Vector3( 0, 0, 0 );
				
				tk2dSprite_char.Play("Run");
			
			}
			else
			{
				
				
			}
		}
		if( animState == AnimState.Run )
		{
			if( translation < 0.1f && translation > -0.1f )
			{	
				animState = AnimState.Turn;
				
				tk2dSprite_char.AnimationCompleted = AnimDone_Turn;
				tk2dSprite_char.AnimationEventTriggered = null;
				tk2dSprite_char.Play("Turn");
			}
		}
		
		
	}
	
	
	protected void  AnimDone_Turn(tk2dSpriteAnimator sprite, tk2dSpriteAnimationClip clipId)
	{
		
		tk2dSprite_char.AnimationCompleted = null;
		tk2dSprite_char.AnimationEventTriggered = null;
		
		if( tk2dSprite_char.Playing )
			tk2dSprite_char.Stop();
		
		tk2dSprite_char.Play("Idle");
		
		
		animState = AnimState.Idle;
	}
	
	
	protected void AnimDone_Shoot(tk2dSpriteAnimator sprite, tk2dSpriteAnimationClip clip) 
	{
		tk2dSprite_char.AnimationCompleted = null;
		tk2dSprite_char.AnimationEventTriggered = null;
		
		if( tk2dSprite_char.Playing )
			tk2dSprite_char.Stop();
		
		tk2dSprite_char.Play("Idle");
		
		animState = AnimState.Idle;
	}

	
<<<<<<< HEAD:GBBasketBall_U5/Assets/Scripts/Character_FT.cs
	protected void ThrowBall ( float y_pos )  // 0 ~ 70
=======
	protected void ThrowBall ( float y_pos,bool isBoss = false )  // 0 ~ 70
>>>>>>> 556bfb9fa037fc5ecd7fcced81762e934c2dce63:GBBasketBall_U5/Assets/Scripts/GameScene/Character_FT.cs
	{
		
		//Debug.Log( "ypos : "+ y_pos );
		
		if( y_pos > 55 )
			y_pos = 55;
		if( y_pos < 30 )
			y_pos = 30f;
			
		Global_FT.PlayClip( "ballshoot", -1 );
		
        clone = Instantiate(ball, new Vector3(transform.position.x, transform.position.y + 3, -20), transform.rotation) as GameObject;
        
		if(isBoss == true){
			clone.tag = "BossBall";
		}
		else{
			clone.tag = "Ball";
		}
		clone.GetComponent<Rigidbody>().isKinematic = false;
		
		clone.GetComponent<Rigidbody>().AddForce(new Vector3( 0, 270 * (( y_pos * 0.7f ) + 35) / 73f , 0 ), ForceMode.Impulse );

	}
	
	public void Boom ( )
	{
		boombtn.gameObject.transform.position = new Vector3( 300f, -70, -35f );
		//GameObject.Find( "BoomBtn" ).transform.position = new Vector3( 300f, -70, -35f );

		if( Global_FT.boom_count < 100 )
			return;
		
		Global_FT.boom_count = 0;
		
		Global_FT.PlayClip( "boom_fallingballs", -1 );


	
		StartCoroutine(waitforsec_boom ( 0.0f ));
		StartCoroutine(waitforsec_boom ( 0.05f ));
		StartCoroutine(waitforsec_boom ( 0.1f ));
		StartCoroutine(waitforsec_boom ( 0.15f ));
		StartCoroutine(waitforsec_boom ( 0.2f ));
		StartCoroutine(waitforsec_boom ( 0.25f ));
		StartCoroutine(waitforsec_boom ( 0.3f ));
		StartCoroutine(waitforsec_boom ( 0.35f ));
		StartCoroutine(waitforsec_boom ( 0.4f ));
		StartCoroutine(waitforsec_boom ( 0.45f ));
		
		
	}
	
	
	public	IEnumerator waitforsec_boom ( float delay )
	{
		yield return new WaitForSeconds(delay);

        clone = Instantiate(ball, new Vector3(transform.position.x + 140 - ((delay+0.05f) * 20) * 25, transform.position.y + 3, -20), transform.rotation) as GameObject;
        
		clone.GetComponent<Rigidbody>().isKinematic = false;
		
		clone.GetComponent<Rigidbody>().AddForce(new Vector3( 0, 270  , 0 ), ForceMode.Impulse );

		
	}
}







