using UnityEngine;
using System.Collections;

public class Board : MonoBehaviour {
	
	public enum BoardType {  
		Normal,
		Wide,
		Slow,
		Time,
		Super,
	}

	public BoardType boardType = BoardType.Normal;
	
	public int boardIndex = 0;
	
	public float speed = 100f;
	public float speed_level_top = 1f;
	public float speed_level_mid = 2f;
	public float speed_level_bottom = 3f;
	

	public tk2dSpriteAnimator board_tk2dSprite;

	tk2dSpriteAnimator[] pScoreSprt;
	
	tk2dSpriteAnimator[] pScoreSprt;
	tk2dSpriteAnimator[] eScoreSprt;
	
	// Use this for initialization
	void Start () {

		pScoreSprt =  new tk2dSpriteAnimator[5];

		pScoreSprt[0] = GameObject.Find("PScore_Number1").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[1] = GameObject.Find("PScore_Number10").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[2] = GameObject.Find("PScore_Number100").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[3] = GameObject.Find("PScore_Number1000").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[4] = GameObject.Find("PScore_Number10000").GetComponent<tk2dSpriteAnimator>();
<<<<<<< HEAD:GBBasketBall_U5/Assets/Scripts/Board.cs
	
=======

		//alternate solution
		if(GameManager_FT.gameMode == GameManager_FT.GameMode.Boss){
			eScoreSprt =  new tk2dSpriteAnimator[5];
			
			eScoreSprt[0] = GameObject.Find("eScore_Number1").GetComponent<tk2dSpriteAnimator>();
			eScoreSprt[1] = GameObject.Find("eScore_Number10").GetComponent<tk2dSpriteAnimator>();
			eScoreSprt[2] = GameObject.Find("eScore_Number100").GetComponent<tk2dSpriteAnimator>();
			eScoreSprt[3] = GameObject.Find("eScore_Number1000").GetComponent<tk2dSpriteAnimator>();
			eScoreSprt[4] = GameObject.Find("eScore_Number10000").GetComponent<tk2dSpriteAnimator>();

		}
>>>>>>> 556bfb9fa037fc5ecd7fcced81762e934c2dce63:GBBasketBall_U5/Assets/Scripts/GameScene/Board.cs
		boardType = BoardType.Normal;

		board_tk2dSprite = gameObject.GetComponent<tk2dSpriteAnimator>();
	}
	
	// Update is called once per frame
	void Update () {
	
		if( Global_FT.bSlowState == true )
			speed = 50f;
		else
<<<<<<< HEAD:GBBasketBall_U5/Assets/Scripts/Board.cs
			speed = 0;
=======
			speed = 100;
>>>>>>> 556bfb9fa037fc5ecd7fcced81762e934c2dce63:GBBasketBall_U5/Assets/Scripts/GameScene/Board.cs
		
		if( boardIndex == 0 )  //top
		{
			float translation = speed;
			translation *=  Time.deltaTime * speed_level_top / 5f;
			
			transform.Translate( translation, 0, 0);
		}	
		else if( boardIndex == 1 ) //mid
		{
			float translation = speed;
			translation *=  Time.deltaTime * speed_level_mid / 5f;
			
			transform.Translate( - translation , 0, 0);
		}	
		else if( boardIndex == 2 ) //bottom
		{
			float translation = speed;
			translation *=  Time.deltaTime * speed_level_bottom / 5f;
			
			transform.Translate( translation, 0, 0);
		}	
		
		
		//public static int BoardCount_Wide = 0;
		//public static int BoardCount_Super = 0;
		//public static int BoardCount_Slow = 0;
		//public static int BoardCount_Time = 0;
		
		if( boardIndex == 0 )  //top
		{
			
			if( transform.position.x > 180f ){
				transform.position = new Vector3( -180f, transform.position.y, transform.position.z );
				
				isEventBoard( );
			}
		}	
		else if( boardIndex == 1 ) //mid
		{
			if( transform.position.x < -180f ){
				transform.position = new Vector3( 180f, transform.position.y, transform.position.z );
				
				isEventBoard( );
			}
		}	
		else if( boardIndex == 2 ) //bottom
		{
			if( transform.position.x > 180f){
				transform.position = new Vector3(-180f, transform.position.y, transform.position.z );

			
				isEventBoard( );
			}
		}

	}
	
	void isEventBoard ()
	{
		if( boardType != BoardType.Normal )
		{
			SetToNormalBoard ();
				
			return;
		}


		int random = Random.Range( 0, 100 );
		if( random < 5 )
		{
			if( Global_FT.BoardCount_Wide < 2 )
			{
				boardType = BoardType.Wide;
//				Debug.Log("boardType : " + boardType );
//				Debug.Log("gameObject="+gameObject.name);
				
				Global_FT.BoardCount_Wide++;

				board_tk2dSprite.Play( "Wide" );
			}
		}
		else if( random < 10)
		{
			if( Global_FT.BoardCount_Slow < 2 )
			{
				boardType = BoardType.Slow;
				
//				Debug.Log("boardType : " + boardType );
//				Debug.Log("gameObject="+gameObject.name);
				
				Global_FT.BoardCount_Slow++;

				board_tk2dSprite.Play( "Slow" );
			}
		}
		else if( random < 15 )
		{
			if( Global_FT.BoardCount_Time < 2 )
			{
				boardType = BoardType.Time;
				
//				Debug.Log("boardType : " + boardType );
//				Debug.Log("gameObject="+gameObject.name);
				
				Global_FT.BoardCount_Time++;

				board_tk2dSprite.Play ( "+Time" );
			}
		}
		else if( random < 16   )
		{
			if( Global_FT.BoardCount_Super < 1)
			{
				boardType = BoardType.Super;
				
//				Debug.Log("boardType : " + boardType );
//				Debug.Log("gameObject="+gameObject.name);
				
				Global_FT.BoardCount_Super++;

				board_tk2dSprite.Play ( "Super" );
			}
		}


		if(boardType != BoardType.Normal && boardIndex != 2){
			Global_FT.BoardPowerUps.Add(this.gameObject);
		}
	}
	
	
<<<<<<< HEAD:GBBasketBall_U5/Assets/Scripts/Board.cs
	public void GoalIn () {
=======
	public void GoalIn (bool isBoss = false) {
>>>>>>> 556bfb9fa037fc5ecd7fcced81762e934c2dce63:GBBasketBall_U5/Assets/Scripts/GameScene/Board.cs

		
		board_tk2dSprite.AnimationCompleted = null;
		board_tk2dSprite.AnimationEventTriggered = null;
		board_tk2dSprite.Play("Goal");
		
<<<<<<< HEAD:GBBasketBall_U5/Assets/Scripts/Board.cs
		Global_FT.PlayerScore += 100;
		
		int number_1 = Mathf.Clamp(Global_FT.PlayerScore % 10, 0, 9);
		int number_10 = Mathf.Clamp(Global_FT.PlayerScore / 10 % 10, 0, 9);
		int number_100 = Mathf.Clamp(Global_FT.PlayerScore / 100 % 10, 0, 9);
		int number_1000 = Mathf.Clamp(Global_FT.PlayerScore / 1000 % 10, 0, 9);
		int number_10000 = Mathf.Clamp(Global_FT.PlayerScore / 10000 % 10, 0, 9);
			
		pScoreSprt[0].Play( number_1.ToString() );
		pScoreSprt[1].Play( number_10.ToString() );
		pScoreSprt[2].Play( number_100.ToString() );
		pScoreSprt[3].Play( number_1000.ToString() );
		pScoreSprt[4].Play( number_10000.ToString() );
=======
		int number_1 = 0;
		int number_10 = 0;
		int number_100  = 0;
		int number_1000 = 0;
		int number_10000  = 0;

		if(!isBoss){
			Global_FT.PlayerScore += 100;

			number_1 = Mathf.Clamp(Global_FT.PlayerScore % 10, 0, 9);
			number_10 = Mathf.Clamp(Global_FT.PlayerScore / 10 % 10, 0, 9);
			number_100 = Mathf.Clamp(Global_FT.PlayerScore / 100 % 10, 0, 9);
			number_1000 = Mathf.Clamp(Global_FT.PlayerScore / 1000 % 10, 0, 9);
			number_10000 = Mathf.Clamp(Global_FT.PlayerScore / 10000 % 10, 0, 9);

			
			pScoreSprt[0].Play( number_1.ToString() );
			pScoreSprt[1].Play( number_10.ToString() );
			pScoreSprt[2].Play( number_100.ToString() );
			pScoreSprt[3].Play( number_1000.ToString() );
			pScoreSprt[4].Play( number_10000.ToString() );
		}
		else{
			Global_FT.AIScore += 100;
			Debug.Log("Score");
			number_1 = Mathf.Clamp(Global_FT.AIScore % 10, 0, 9);
			number_10 = Mathf.Clamp(Global_FT.AIScore / 10 % 10, 0, 9);
			number_100 = Mathf.Clamp(Global_FT.AIScore / 100 % 10, 0, 9);
			number_1000 = Mathf.Clamp(Global_FT.AIScore / 1000 % 10, 0, 9);
			number_10000 = Mathf.Clamp(Global_FT.AIScore / 10000 % 10, 0, 9);

			
			eScoreSprt[0].Play( number_1.ToString() );
			eScoreSprt[1].Play( number_10.ToString() );
			eScoreSprt[2].Play( number_100.ToString() );
			eScoreSprt[3].Play( number_1000.ToString() );
			eScoreSprt[4].Play( number_10000.ToString() );
		}
			
>>>>>>> 556bfb9fa037fc5ecd7fcced81762e934c2dce63:GBBasketBall_U5/Assets/Scripts/GameScene/Board.cs
		
		StartCoroutine(waitforsec_goalin ( 0.1f ));
		
	}
	
	
	public	IEnumerator waitforsec_goalin ( float delay )
	{
		yield return new WaitForSeconds(delay);
	
		
		if(boardType != BoardType.Normal){
			Global_FT.BoardPowerUps.Remove(this.gameObject);
		}
	
		if( boardType == BoardType.Wide ){
			Global_FT.BoardCount_Wide--;
			
			Global_FT.time_wide = 10f;
			
			Global_FT.bWideState = true;
			
			
			GameObject[] objlist;
		   	objlist = GameObject.FindGameObjectsWithTag("Rim");
			for( int i=0; i<objlist.Length; i++ )
			{
				objlist[i].SendMessage( "OnBecomeWide", SendMessageOptions.DontRequireReceiver );
			}
			
			
		}else if( boardType == BoardType.Slow ){
			Global_FT.BoardCount_Slow--;
			
			Global_FT.time_slow = 10f;
			
			Global_FT.bSlowState = true;
			GameObject.Find("BGM").GetComponent<BGM_FT>().Stop_BGM();
			GameObject.Find("BGM").GetComponent<BGM_FT>().Start_BGM();
			
//			Debug.Log("bSlowState == true");
			
//			Debug.Log("gameObject="+gameObject.name);
			
		}else if( boardType == BoardType.Time ){
			Global_FT.BoardCount_Time--;
			
			Global_FT.PlayClip( "timeget", -1 );
			
			Global_FT.time_game += 20f;
			if( Global_FT.time_game > 120f )
				Global_FT.time_game = 120f;
			
		}else if( boardType == BoardType.Super ){
			Global_FT.BoardCount_Super--;
			
			Global_FT.time_super = 10f;
			Global_FT.time_superbg = 0.1f;
			Global_FT.index_superbg = 0;
			
			Global_FT.bSuperState = true;
		}
		
		boardType = BoardType.Normal;
		

		
		board_tk2dSprite.AnimationCompleted = null;
		board_tk2dSprite.AnimationEventTriggered = null;
		
		if( board_tk2dSprite.Playing )
			board_tk2dSprite.Stop();
		
		board_tk2dSprite.Play("Normal");


		
	}
	
	void SetToNormalBoard ( )
	{
		
		
		if( boardType == BoardType.Wide ){
			Global_FT.BoardCount_Wide--;
			
		}else if( boardType == BoardType.Slow ){
			Global_FT.BoardCount_Slow--;
			
		}else if( boardType == BoardType.Time ){
			Global_FT.BoardCount_Time--;
			
		}else if( boardType == BoardType.Super ){
			Global_FT.BoardCount_Super--;
			
		}
		boardType = BoardType.Normal;
		

		
		board_tk2dSprite.AnimationCompleted = null;
		board_tk2dSprite.AnimationEventTriggered = null;
		
		if( board_tk2dSprite.Playing )
			board_tk2dSprite.Stop();
		
		board_tk2dSprite.Play("Normal");

		Global_FT.BoardPowerUps.Remove(this.gameObject);
	}
	
}








