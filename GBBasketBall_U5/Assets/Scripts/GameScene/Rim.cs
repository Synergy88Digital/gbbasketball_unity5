using UnityEngine;
using System.Collections;

public class Rim : MonoBehaviour {
	
	public GameObject objBoard;

	public tk2dSpriteAnimator board_tk2dSprite;
	public tk2dSpriteAnimator rim_tk2dSprite;
	
 	bool bGoaled = false;
	bool bRamed = false;
	bool bEnterFromTop = false;
	
	void OnBecomeWide () {
	
		if( gameObject.name == "RingLeft" )
		{
			gameObject.GetComponent<BoxCollider>().center = new Vector3( -8, 0, 0 );
		}
		else if(  gameObject.name == "RingRight" )
		{
			gameObject.GetComponent<BoxCollider>().center = new Vector3( 8, 0, 0 );
		}
		else  //center
		{
			this.transform.localScale = new Vector3(1,.1f,1);
			rim_tk2dSprite.AnimationCompleted = null;
			rim_tk2dSprite.AnimationEventTriggered = null;
			rim_tk2dSprite.Play( "Wide_Rim" );
		}
		
	}
	
	void OnBecomeNormal () {
		
		if( gameObject.name == "RingLeft" )
		{
			gameObject.GetComponent<BoxCollider>().center = new Vector3( 0, 0, 0 );	
		}
		else if(  gameObject.name == "RingRight" )
		{
			gameObject.GetComponent<BoxCollider>().center = new Vector3( 0, 0, 0 );
		}
		else  //center
		{
			
			this.transform.localScale = new Vector3(.1f,.1f,1);
			rim_tk2dSprite.AnimationCompleted = null;
			rim_tk2dSprite.AnimationEventTriggered = null;
			rim_tk2dSprite.Play( "Normal_Rim" );
		}
		
	}
	
	void OnCollisionEnter(Collision collision) {


		if( bGoaled )
			return;
		
		if( bRamed )
			return;
		
		bRamed = true;
		
		
		if( objBoard.GetComponent<Board>().boardType != Board.BoardType.Normal )
			Global_FT.PlayClip( "specialringsound", -1 );
		else
       		Global_FT.PlayClip( "ballbounce", -1 );
		

		
		rim_tk2dSprite.Stop();
		
		rim_tk2dSprite.AnimationCompleted = null;
		rim_tk2dSprite.AnimationEventTriggered = null;
		if( Global_FT.bWideState == true )
			rim_tk2dSprite.Play( "Wide_Rim_NoGoal" );
		else
			rim_tk2dSprite.Play( "Normal_Rim_NoGoal" );
		
		
		StartCoroutine(waitforsec_ram ( 0.5f ));
    }
	
	public	IEnumerator waitforsec_ram ( float delay )
	{
		yield return new WaitForSeconds(delay);
	

		rim_tk2dSprite.Stop();
		
		rim_tk2dSprite.AnimationCompleted = null;
		rim_tk2dSprite.AnimationEventTriggered = null;
		if( Global_FT.bWideState == true )
			rim_tk2dSprite.Play( "Wide_Rim" );
		else
			rim_tk2dSprite.Play( "Normal_Rim" );
		
		
		bRamed = false;
	}

	void OnTriggerExit(Collider ball){

<<<<<<< HEAD:GBBasketBall_U5/Assets/Scripts/Rim.cs
		Debug.Log("Trigger Exit");

=======
>>>>>>> 556bfb9fa037fc5ecd7fcced81762e934c2dce63:GBBasketBall_U5/Assets/Scripts/GameScene/Rim.cs
		if(bGoaled)
			return;


		if(ball.transform.position.y >= this.transform.position.y)
		{
			Debug.Log("Ball Spilt Out");
			//ball entered from top by was spilled out
			bEnterFromTop = false;
			return;
		}
		if(!bEnterFromTop)
			return;

		bGoaled = true;
		
		Global_FT.PlayClip( "goalin", -1 );
		
		
		StartCoroutine(waitforsec_goalin_ani ( 0.2f ));
		
		StartCoroutine(waitforsec_goalin ( 1.0f ));
		
		
		//SendMessage
		//objBoard.transform.SendMessage( "GoalIn" );
		if(ball.tag == "Ball"){
			objBoard.GetComponent<Board>().GoalIn();
		}
		else if(ball.tag == "BossBall"){
			objBoard.GetComponent<Board>().GoalIn(true);
		}
		
		bEnterFromTop = false;
<<<<<<< HEAD:GBBasketBall_U5/Assets/Scripts/Rim.cs
		Debug.Log("Score");
=======

>>>>>>> 556bfb9fa037fc5ecd7fcced81762e934c2dce63:GBBasketBall_U5/Assets/Scripts/GameScene/Rim.cs
		ball.transform.SendMessage("BallGoalIn", SendMessageOptions.DontRequireReceiver );


	}
	
	void OnTriggerEnter(Collider ball) {

<<<<<<< HEAD:GBBasketBall_U5/Assets/Scripts/Rim.cs
		Debug.Log("Trigger Enter");

=======
>>>>>>> 556bfb9fa037fc5ecd7fcced81762e934c2dce63:GBBasketBall_U5/Assets/Scripts/GameScene/Rim.cs
		//prevents scoring by the Ball hitting the collider from below
		if(this.transform.position.y >= ball.transform.position.y)
			return;

		bEnterFromTop = true;


	}


	
	public	IEnumerator waitforsec_goalin_ani ( float delay )
	{
		yield return new WaitForSeconds(delay);
		
		rim_tk2dSprite.AnimationCompleted = null;
		rim_tk2dSprite.AnimationEventTriggered = null;
		if( rim_tk2dSprite.Playing )
			rim_tk2dSprite.Stop ();
		
		rim_tk2dSprite.AnimationCompleted = AnimDone_Goalin;
		rim_tk2dSprite.AnimationEventTriggered = null;
		if( Global_FT.bWideState == true )
			rim_tk2dSprite.Play( "Wide_Rim_Goal" );
		else
			rim_tk2dSprite.Play( "Normal_Rim_Goal" );
	}
	
	void AnimDone_Goalin(tk2dSpriteAnimator sprite, tk2dSpriteAnimationClip clip) 
	{
		rim_tk2dSprite.AnimationCompleted = null;
		rim_tk2dSprite.AnimationEventTriggered = null;
		
		if( rim_tk2dSprite.Playing )
			rim_tk2dSprite.Stop();
		
		if( Global_FT.bWideState == true )
			rim_tk2dSprite.Play("Wide_Rim");
		else
			rim_tk2dSprite.Play("Normal_Rim");
	}
	
	public	IEnumerator waitforsec_goalin ( float delay )
	{
		yield return new WaitForSeconds(delay);
		
		bGoaled = false;

	}
	
}
