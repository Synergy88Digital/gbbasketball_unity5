using UnityEngine;
using System.Collections;

public class GameManager_FT : MonoBehaviour {
	
	public enum GameState {  
		Start,
		Playing,
		GameOver,
		Restart,
		Pause,
	}

	public enum GameMode{
		League,
		Boss,
		Arcade,

	}
	
	public static GameState gameState = GameState.Pause;
	public static GameMode gameMode;
	
	public GameObject combo_image;
	public GameObject combo_Grp;
	public GameObject combo_Number1;
	public GameObject combo_Number10;
	public GameObject combo_Number100;


	public GameObject combo_plus;
	
	public GameObject bg;
	public GameObject bg_super1;
	public GameObject bg_super2;
	public GameObject bg_super3;
	public GameObject bg_super4;
	
	public GameObject timebar_wide;
	public GameObject timebar_widegauge;
	public GameObject timebar_slow;
	public GameObject timebar_slowgauge;
	public GameObject timebar_super;
	public GameObject timebar_supergauge;


	public GameObject SubPanel;
	public GameObject PausePanel;
	public GameObject GameOverPanel;
	
	public tk2dUIItem Pause;
	public tk2dUIItem retry;
	public tk2dUIItem unPause;	
	public tk2dUIItem returnMap;
	public tk2dUIItem MuteSound;

	public tk2dUIItem fbShare;

	public TextMesh GameResult;
	public TextMesh Score;
	public TextMesh MedalsCollected;

	//public tk2dTextMesh GameResult;
	//public tk2dTextMesh MedalsCollected;
	//public tk2dTextMesh Score;

	public tk2dSprite[] StarsCollected;

	tk2dSpriteAnimator[] pScoreSprt;
	tk2dSpriteAnimator[] eScoreSprt;
	tk2dSpriteAnimator[] TScoreSprt;

	[SerializeField]
	private GameObject BossBattleUI;
	[SerializeField]
	private GameObject SinglePlayerUI;
	[SerializeField]
	private GameObject ArcadeUI;

	private CurLevelInfo levelInfo;

	//boss Battle

	public GameObject Boss;

		
	void OnEnable()
	{		
		Pause.OnClick += PauseGame;
		retry.OnClick += RestartGame;
		unPause.OnClick += upPauseGame;
		returnMap.OnClick += returnToMap;
    }

    void OnDisable()
	{
		Pause.OnClick -= PauseGame;
		retry.OnClick -= RestartGame;
		unPause.OnClick -= upPauseGame;
		returnMap.OnClick -= returnToMap;
    }
	
		
	void Awake() {
		gameState = GameState.Start;
		if(GameObject.Find("Game Info") != null)
			levelInfo = GameObject.Find("Game Info").GetComponent<CurLevelInfo>();


		if(levelInfo == null){ // Arcade
			gameMode = GameMode.Arcade;
		}
		else if(levelInfo.isBossBattle){ // Boss
			gameMode = GameMode.Boss;
		}
		else if(!levelInfo.isBossBattle){ // League
			gameMode = GameMode.League;
		}

		InitScreenUI();

		Global_FT.time_game = 120f;
		
		Time.timeScale = 1.0f;
		iTween.ScaleTo( combo_Grp, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 0f, "easetype", "linear" ) );
		iTween.MoveTo( combo_Grp, iTween.Hash("x", 25.0f, "y", 3.0f, "z", -20.0f, "time", 0.1f, "delay", 0f, "easetype", "linear" ) );
		
	}

	void InitScreenUI(){
		//if Level Info is not found set Game to Arcade
		if(gameMode != GameMode.Arcade){

			if(gameMode == GameMode.Boss){
				
				BossBattleUI.SetActive(true);

				eScoreSprt =  new tk2dSpriteAnimator[5];
				
				eScoreSprt[0] = GameObject.Find("eScore_Number1").GetComponent<tk2dSpriteAnimator>();
				eScoreSprt[1] = GameObject.Find("eScore_Number10").GetComponent<tk2dSpriteAnimator>();
				eScoreSprt[2] = GameObject.Find("eScore_Number100").GetComponent<tk2dSpriteAnimator>();
				eScoreSprt[3] = GameObject.Find("eScore_Number1000").GetComponent<tk2dSpriteAnimator>();
				eScoreSprt[4] = GameObject.Find("eScore_Number10000").GetComponent<tk2dSpriteAnimator>();

				Boss.SetActive(true);
			}
			else if(gameMode == GameMode.League){

				SinglePlayerUI.SetActive(true);	

				TScoreSprt = new tk2dSpriteAnimator[5];
				
				TScoreSprt[0] = GameObject.Find("Score_Number1").GetComponent<tk2dSpriteAnimator>();
				TScoreSprt[1] = GameObject.Find("Score_Number10").GetComponent<tk2dSpriteAnimator>();
				TScoreSprt[2] = GameObject.Find("Score_Number100").GetComponent<tk2dSpriteAnimator>();
				TScoreSprt[3] = GameObject.Find("Score_Number1000").GetComponent<tk2dSpriteAnimator>();
				TScoreSprt[4] = GameObject.Find("Score_Number10000").GetComponent<tk2dSpriteAnimator>();

			}

		}
		else if(gameMode == GameMode.Arcade){
			ArcadeUI.SetActive(true);
		}

		pScoreSprt =  new tk2dSpriteAnimator[6];
		
		pScoreSprt[0] = GameObject.Find("PScore_Number1").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[1] = GameObject.Find("PScore_Number10").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[2] = GameObject.Find("PScore_Number100").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[3] = GameObject.Find("PScore_Number1000").GetComponent<tk2dSpriteAnimator>();
		pScoreSprt[4] = GameObject.Find("PScore_Number10000").GetComponent<tk2dSpriteAnimator>();
		
		
		StartCoroutine(InitGame());
	
	}
	IEnumerator InitGame(){
			
		
		yield return new WaitForEndOfFrame();

		if(gameMode == GameMode.League){
			int number_1 = Mathf.Clamp(levelInfo.TargetScore % 10, 0, 9);
			int number_10 = Mathf.Clamp(levelInfo.TargetScore / 10 % 10, 0, 9);
			int number_100 = Mathf.Clamp(levelInfo.TargetScore / 100 % 10, 0, 9);
			int number_1000 = Mathf.Clamp(levelInfo.TargetScore / 1000 % 10, 0, 9);
			int number_10000 = Mathf.Clamp(levelInfo.TargetScore / 10000 % 10, 0, 9);
			
			TScoreSprt[0].Play( number_1.ToString() );
			TScoreSprt[1].Play( number_10.ToString() );
			TScoreSprt[2].Play( number_100.ToString() );
			TScoreSprt[3].Play( number_1000.ToString() );
			TScoreSprt[4].Play( number_10000.ToString() );
		}

		yield return new WaitForSeconds(3);
		gameState = GameState.Playing;


	}

	// Update is called once per frame
	void Update () {

		AdjustDifficulty ();
		if(gameState == GameState.Playing){
			GameControl();
		}
	
	}

	void GameControl(){
		Global_FT.time_game -= Time.deltaTime;
		
		if( Global_FT.time_game < 0 )
		{
			Global_FT.time_game = 0;
			gameState = GameState.GameOver;	
			
			Time.timeScale = 0;
			if(levelInfo != null){
				if(Global_FT.PlayerScore >= levelInfo.TargetScore){
					LoadGameOverLeague(true);
				}
				else{
					LoadGameOverLeague(false);
				}
			}
			else{
				//arcade mode
				LoadGameOverLeague(true);

			}
		}
		
		if( Global_FT.bWideState == true )
		{
			Global_FT.time_wide -= Time.deltaTime;
			if( Global_FT.time_wide  < 0 )
			{
				Global_FT.time_wide = -1;
				
				Global_FT.bWideState = false;
				
				GameObject[] objlist;
				objlist = GameObject.FindGameObjectsWithTag("Rim");
				for( int i=0; i<objlist.Length; i++ )
				{
					objlist[i].SendMessage( "OnBecomeNormal", SendMessageOptions.DontRequireReceiver );
				}
				
				timebar_wide.SetActive(false);	
				timebar_widegauge.SetActive(false);	
				timebar_widegauge.transform.localScale = new Vector3( 10, 10, 1 );
			}
			else
			{
				if( timebar_wide.active == false )
				{
					timebar_wide.SetActive(true);	
					timebar_widegauge.SetActive(true);	
				}
				
				timebar_widegauge.transform.localScale = new Vector3( Global_FT.time_wide / 10f * 10, 10, 1 );
			}
		}
		
		if( Global_FT.bSlowState == true )
		{
			Global_FT.time_slow -= Time.deltaTime;
			if( Global_FT.time_slow  < 0 )
			{
				Global_FT.time_slow = -1;
				
				Global_FT.bSlowState = false;
				
				GameObject.Find("BGM").GetComponent<BGM_FT>().Stop_BGM();
				GameObject.Find("BGM").GetComponent<BGM_FT>().Start_BGM();
				
				
				timebar_slow.SetActive(false);	
				timebar_slowgauge.SetActive(false);	
				timebar_slowgauge.transform.localScale = new Vector3( 10, 10, 1 );
			}
			else
			{
				if( timebar_slow.active == false )
				{
					timebar_slow.SetActive(true);	
					timebar_slowgauge.SetActive(true);	
				}
				
				timebar_slowgauge.transform.localScale = new Vector3( Global_FT.time_slow / 10f * 10, 10, 1 );
			}
		}
		
		if( Global_FT.bSuperState == true )
		{
			
			Global_FT.time_superbg -= Time.deltaTime;
			if( Global_FT.time_superbg  < 0 )
			{
				Global_FT.time_superbg = 0.1f;
				
				Global_FT.index_superbg++;
				if( Global_FT.index_superbg > 3)
					Global_FT.index_superbg = 0;
				
				this.bg.SetActive(false);
				this.bg_super1.SetActive(false);
				this.bg_super2.SetActive(false);
				this.bg_super3.SetActive(false);
				this.bg_super4.SetActive(false);
				
				if( Global_FT.index_superbg == 0 )
					this.bg_super1.SetActive(true);
				else if( Global_FT.index_superbg == 1 )
					this.bg_super2.SetActive(true);
				else if( Global_FT.index_superbg == 2 )
					this.bg_super3.SetActive(true);
				else if( Global_FT.index_superbg == 3 )
					this.bg_super4.SetActive(true);
			}
			
			Global_FT.time_super -= Time.deltaTime;
			if( Global_FT.time_super  < 0 )
			{
				Global_FT.time_super = -1;
				
				Global_FT.bSuperState = false;
				
				this.bg.SetActive(true);
				this.bg_super1.SetActive(true);
				this.bg_super2.SetActive(true);
				this.bg_super3.SetActive(true);
				this.bg_super4.SetActive(true);
				
				timebar_super.SetActive(false);	
				timebar_supergauge.SetActive(false);	
				timebar_supergauge.transform.localScale = new Vector3( 10, 10, 1 );
				
			}
			else
			{
				
				if( timebar_super.active == false )
				{
					timebar_super.SetActive(true);	
					timebar_supergauge.SetActive(true);	
				}
				
				timebar_supergauge.transform.localScale = new Vector3( Global_FT.time_super , 10, 1 );
			}
		}

	}

	void LoadGameOverLeague(bool isWin = false){

		if(isWin){
			GameResult.text = "Level Complete!";
			MedalsCollected.text = "Medals: 1000";
			
			fbShare.gameObject.transform.localPosition = new Vector3(0,-35,-51);

			foreach(tk2dSprite star in StarsCollected){
				
				star.SetSprite("Obj - Star 00");
			}
		}
		else{
			GameResult.text = "Try Again!";
			MedalsCollected.text = "";

			fbShare.gameObject.transform.localPosition = new Vector3(0,-35,0);	

			foreach(tk2dSprite star in StarsCollected){

				star.SetSprite("Obj - Star 01");
			}
		}
		GameOverPanel.transform.position = new Vector3(0, 0, 0);
		SubPanel.transform.position = new Vector3(0, 0, -55);

		Score.text = "Score: "+Global_FT.PlayerScore;
	}

	public void AdjustDifficulty(){
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			Global_FT.gameDif = Global_FT.Difficulty.Easy;
		}
		else if (Input.GetKeyDown(KeyCode.Alpha2)) {
			Global_FT.gameDif = Global_FT.Difficulty.Norm;
		} 

		else if (Input.GetKeyDown(KeyCode.Alpha3)) {
			Global_FT.gameDif = Global_FT.Difficulty.Hard;
		} 
	}	
	
	public void DispCombo ( int combo_count ) {
	

		Global_FT.PlayerScore += ( combo_count - 1) * 400;
		
		int number_1 = Mathf.Clamp(Global_FT.PlayerScore % 10, 0, 9);
		int number_10 = Mathf.Clamp(Global_FT.PlayerScore / 10 % 10, 0, 9);
		int number_100 = Mathf.Clamp(Global_FT.PlayerScore / 100 % 10, 0, 9);
		int number_1000 = Mathf.Clamp(Global_FT.PlayerScore / 1000 % 10, 0, 9);
		int number_10000 = Mathf.Clamp(Global_FT.PlayerScore / 10000 % 10, 0, 9);

			
		pScoreSprt[0].Play( number_1.ToString() );
		pScoreSprt[1].Play( number_10.ToString() );
		pScoreSprt[2].Play( number_100.ToString() );
		pScoreSprt[3].Play( number_1000.ToString() );
		pScoreSprt[4].Play( number_10000.ToString() );



		number_1 = Mathf.Clamp(combo_count*100 % 10, 0, 9);
		number_10 = Mathf.Clamp(combo_count*100 / 10 % 10, 0, 9);
		number_100 = Mathf.Clamp(combo_count*100 / 100 % 10, 0, 9);

		
		
		combo_Number1.GetComponent<tk2dSpriteAnimator>().Play( number_1.ToString() );
		combo_Number10.GetComponent<tk2dSpriteAnimator>().Play( number_10.ToString() );
		combo_Number100.GetComponent<tk2dSpriteAnimator>().Play( number_100.ToString() );

		iTween.ScaleTo(combo_Grp, iTween.Hash("x", 10.0f, "y", 10.0f, "z", 1.0f, "time", 0.1f, "delay", 0.0f, "easetype", "linear" ) );
		iTween.ScaleTo(combo_Grp, iTween.Hash("x", 0.0f, "y", 0.0f, "z", 0.0f, "time", 0.1f, "delay", 1.0f, "easetype", "linear" ) );
		
	}

	
	void returnToMap(){
		
		Time.timeScale = 1;
		Destroy(levelInfo.gameObject);
		Application.LoadLevel("_Main");
		
	}
	
	void upPauseGame(){


		PausePanel.transform.position = new Vector3(0, 300, -51);
		SubPanel.transform.position = new Vector3(0, 300, -51);
		//gameState = GameState.Playing;
		
		Time.timeScale = 1;
	}
	
	void PauseGame(){
		
		PausePanel.transform.position = new Vector3(0, 0,0);
		SubPanel.transform.position = new Vector3(0, 0,-55);
		
		//gameState = GameState.Pause;
		
		Time.timeScale = 0;
	}
	
	void RestartGame ()
	{		
		Time.timeScale = 1;
		Application.LoadLevelAsync("_Game");

	}

}
