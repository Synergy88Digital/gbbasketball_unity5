using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Global_FT : MonoBehaviour {

	public enum Difficulty
	{
		Easy,
		Norm,
		Hard
	}

	public static Difficulty gameDif = Difficulty.Easy;

	public static GameObject soundManager;
	
	public static bool bSoundOn = true;
	
	public static int GameLevel = 1;
	
	public static int PlayerScore = 0;
	public static int AIScore = 0;

	public static bool[] bGameDone;
	
	public static float BGM_Volume = 1.0f;
	public static float FX_Volume = 1.0f;
	
	static public AudioSource audiosource1;
	static public AudioSource audiosource2;
	static public AudioSource audiosource3;
	static public AudioSource audiosource4;
	static public AudioSource audiosource5;
	static public AudioSource audiosource6;
	static public AudioSource audiosource7;
	static public AudioSource audiosource8;
	static public AudioSource audiosource9;
	static public AudioSource audiosource10;
	static public AudioSource audiosource11;
	static public AudioSource audiosource12;
	
	static public AudioClip clip;
	
	public static int BoardCount_Wide = 0;
	public static int BoardCount_Super = 0;
	public static int BoardCount_Slow = 0;
	public static int BoardCount_Time = 0;

	public static List<GameObject> BoardPowerUps = new List<GameObject>();
	
	public static float time_game = 0f;
	public static float boom_count = 0f;
	
	public static float time_slow = 0f;
	public static float time_wide = 0f;
	public static float time_super = 0f;
	public static float time_superbg = 0f;
	public static int index_superbg = 0;
	
	public static bool bSlowState = false;
	public static bool bSuperState = false;
	public static bool bWideState = false;
	
	
	void Awake ()
	{
   		DontDestroyOnLoad (this);
		
	} 
	
	void Start()
	{
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("Global");
		if( objlist.Length > 1 ){
			DestroyImmediate( gameObject );
			return;
		}
		
		bGameDone = new bool[20];
		for( int i=0; i<20; i++)
			
			bGameDone[i] = false;
		
	}
	
	static public void SetGameDone( int index, bool bDone )
	{
		bGameDone[ index ] = bDone;
		
		for ( int i=0; i<20; i++)
		{
			Debug.Log( "Global.bGameDone["+i+"] = "+Global_FT.bGameDone[ i ] );	
		}
		
	}
	
	
	static public void PlayClip(string clipName, int channelIdx)
	{
		//int play_channel = -1;
		
		//if( Global.FXVOLUME == 0)
		//	return;	
		if(audiosource1 == null)
		audiosource1 = GameObject.Find("SoundSource1").GetComponent<AudioSource>();
		if(audiosource2 == null)
		audiosource2 = GameObject.Find("SoundSource2").GetComponent<AudioSource>();
		if(audiosource3 == null)
		audiosource3 = GameObject.Find("SoundSource3").GetComponent<AudioSource>();
		if(audiosource4 == null)
		audiosource4 = GameObject.Find("SoundSource4").GetComponent<AudioSource>();
		if(audiosource5 == null)
		audiosource5 = GameObject.Find("SoundSource5").GetComponent<AudioSource>();
		if(audiosource6 == null)
		audiosource6 = GameObject.Find("SoundSource6").GetComponent<AudioSource>();
		if(audiosource7 == null)
		audiosource7 = GameObject.Find("SoundSource7").GetComponent<AudioSource>();
		if(audiosource8 == null)
		audiosource8 = GameObject.Find("SoundSource8").GetComponent<AudioSource>();
		if(audiosource9 == null)
		audiosource9 = GameObject.Find("SoundSource9").GetComponent<AudioSource>();
		if(audiosource10 == null)
		audiosource10 = GameObject.Find("SoundSource10").GetComponent<AudioSource>();
		if(audiosource11 == null)
		audiosource11 = GameObject.Find("SoundSource11").GetComponent<AudioSource>();
		if(audiosource12 == null)
		audiosource12 = GameObject.Find("SoundSource12").GetComponent<AudioSource>();
		
		clip = Resources.Load("FX/"+clipName) as AudioClip;
		
		if( channelIdx == -1 )
		{
			
			if( audiosource1.isPlaying == false )
			{
				audiosource1.volume = (0.7f * FX_Volume );
				audiosource1.clip = clip;
				audiosource1.Play();
			}
			else if( audiosource2.isPlaying == false )
			{
				audiosource2.volume = (0.7f * FX_Volume );
				audiosource2.clip = clip;
				audiosource2.Play();
			}
			else if( audiosource3.isPlaying == false )
			{
				audiosource3.volume = (0.7f * FX_Volume);
				audiosource3.clip = clip;
				audiosource3.Play();
			}
			else if( audiosource4.isPlaying == false )
			{
				audiosource4.volume = (0.7f * FX_Volume);
				audiosource4.clip = clip;
				audiosource4.Play();
			}
			else if( audiosource5.isPlaying == false )
			{
				audiosource5.volume = (0.7f * FX_Volume);
				audiosource5.clip = clip;
				audiosource5.Play();
			}
			else if( audiosource6.isPlaying == false )
			{
				audiosource6.volume = (0.7f * FX_Volume);
				audiosource6.clip = clip;
				audiosource6.Play();
			}
			else 
			{
				audiosource7.volume = (0.7f * FX_Volume);
				audiosource7.clip = clip;
				audiosource7.Play();
			}
			
		}
		else
		{
			if( channelIdx == 8 )
			{
				audiosource8.volume = (0.7f * FX_Volume);
				audiosource8.clip = clip;
				audiosource8.Play();
			}
			else if( channelIdx == 9 )
			{
				audiosource9.volume = (1.0f * FX_Volume);
				audiosource9.clip = clip;
				audiosource9.Play();
			}
			else if( channelIdx == 10 )
			{
				audiosource10.volume = (1.0f * FX_Volume);
				audiosource10.clip = clip;
				audiosource10.Play();
			}
			else if( channelIdx == 11 )
			{
				audiosource11.volume = (1.0f * FX_Volume);
				audiosource11.clip = clip;
				audiosource11.Play();
			}
			else if( channelIdx == 12 )
			{
				audiosource12.volume = (0.5f * FX_Volume);
				audiosource12.clip = clip;
				audiosource12.Play();
			}
		}
		
	}
	
	static public void StopClip(int channelIdx)
	{
		if( channelIdx == 10 )
		{
			audiosource10.volume = 0;
			audiosource10.Stop();
		}
	}
}
