// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class BGM_FT : MonoBehaviour {
	
	public AudioSource AudioSource_BGM;
	
	private bool bFadeDone = false;
	private bool bStopForce = false;
	
	void  Awake (){
	
	//	DontDestroyOnLoad (transform.gameObject);
	}
	
	void Start()
	{
//		Global.InitStage( );
		
		
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("BGM");
		if( objlist.Length > 1 ){
			DestroyImmediate( gameObject );
			return;
		}
		
		AudioSource_BGM = GameObject.Find("BGM").GetComponent<AudioSource>();
		
		Stop_BGM();
		
		Start_BGM();
	}
	
	public void Start_BGM()
	{
		
		
		//Global ptrGlobal = (Global)GameObject.Find("Global").GetComponent( typeof(Global) );
		//ptrGlobal.LoadOption();
		

		AudioSource_BGM = GameObject.Find("BGM").GetComponent<AudioSource>();
			
		if( AudioSource_BGM.isPlaying == true )
			return;
		
		if( GameObject.Find("BGM") != null )
		{
			
			//AudioSource_BGM.volume = 0.0f;
			//AudioSource_BGM.mute = true;
			
			
			if( Application.loadedLevelName == "_game_Throw" )
			{
				if( Global_FT.time_slow > 0 )
				{
					AudioClip clip = Resources.Load("BGM/"+"bgm-slow") as AudioClip;
					AudioSource_BGM.clip = clip;
				}
				else
				{
					AudioClip clip = Resources.Load("BGM/"+"bgm-normal") as AudioClip;
					AudioSource_BGM.clip = clip;
				}
			}
			else
			{
				AudioClip clip = Resources.Load("BGM/"+"bgm-intro-1") as AudioClip;
				AudioSource_BGM.clip = clip;
			}		
			
			
			if( AudioSource_BGM.isPlaying != true )
			{
				AudioSource_BGM.mute = false;
				AudioSource_BGM.volume = (0.5f * Global_FT.BGM_Volume );
				AudioSource_BGM.loop = true;
				AudioSource_BGM.Play();
			}
		}
		
	}
	
	public void Start_BossBGM()
	{

		AudioSource_BGM = GameObject.Find("BGM").GetComponent<AudioSource>();
			
		if( AudioSource_BGM.isPlaying == true )
			return;
		
		if( GameObject.Find("BGM") != null )
		{
			

			AudioClip clip = Resources.Load("BGM/"+"boss_bgm") as AudioClip;
				
			AudioSource_BGM.clip = clip;
			
			
			if( AudioSource_BGM.isPlaying != true )
			{
				AudioSource_BGM.mute = false;
				AudioSource_BGM.volume = (0.5f * Global_FT.BGM_Volume );
				AudioSource_BGM.loop = true;
				AudioSource_BGM.Play();
			}
		}
		
	}
	
	
	public void Stop_BGM()
	{
		bStopForce = true;
		
		if( AudioSource_BGM.isPlaying == true)
		{
			AudioSource_BGM.mute = true;
			AudioSource_BGM.volume = 0.0f;
			AudioSource_BGM.Stop();
		}
	}
	
	public void Faid_BGM2()
	{
		if( AudioSource_BGM.isPlaying == true)
		{
			bFadeDone = false;
				
			Hashtable ht0 = new Hashtable();
			ht0.Add("from", 0.5f);
			ht0.Add("to", 0.0f);
			ht0.Add("time", 1.5f);
			ht0.Add("onupdate", "updateValueto_Mute2");
			ht0.Add("easetype", "linear");
			iTween.ValueTo(gameObject, ht0);
		}
	}
	
	void updateValueto_Mute2(float theValue)
	{
		//print( theValue );

		if( theValue < 0.1 )
		{
			if( bFadeDone == false )
			{
				bFadeDone = true;
				
				AudioSource_BGM.mute = true;
				AudioSource_BGM.volume = 0.0f;
				AudioSource_BGM.Stop();
			}
		}
		else
			AudioSource_BGM.volume = theValue;
		
	}
	
	
	public void Faid_BGM()
	{
		bFadeDone = false;
		bStopForce = false;
		
		if( AudioSource_BGM.isPlaying == true)
		{
			Hashtable ht0 = new Hashtable();
			ht0.Add("from", 0.5f);
			ht0.Add("to", 0.0f);
			ht0.Add("time", 5.0f);
			ht0.Add("onupdate", "updateValueto_Mute");
			ht0.Add("easetype", "linear");
			iTween.ValueTo(gameObject, ht0);
		}
	}
	
	void updateValueto_Mute(float theValue)
	{
		//print( theValue );
		
		if( bStopForce == true ){
		
			AudioSource_BGM.mute = true;
			AudioSource_BGM.volume = 0.0f;
			AudioSource_BGM.Stop();
			
			bStopForce = false;
		}
		
		if( theValue < 0.1 )
		{
			if( bFadeDone == false )
			{
				bFadeDone = true;
				
				AudioSource_BGM.mute = true;
				AudioSource_BGM.volume = 0.0f;
				AudioSource_BGM.Stop();
			}
		}
		else
			AudioSource_BGM.volume = theValue;
		
	}
	
	public void FaidAndPlay_BGM()
	{
		bFadeDone = false;
		bStopForce = false;
		
		if( AudioSource_BGM.isPlaying == true)
		{
			Hashtable ht0 = new Hashtable();
			ht0.Add("from", 0.5f);
			ht0.Add("to", 0.0f);
			ht0.Add("time", 1.0f);
			ht0.Add("onupdate", "updateValueto_MutePlay");
			ht0.Add("easetype", "linear");
			iTween.ValueTo(gameObject, ht0);
		}
		else
			Start_BGM();
	}
	
	void updateValueto_MutePlay(float theValue)
	{
		
		if( theValue < 0.1 )
		{
			if( bFadeDone == false )
			{
				bFadeDone = true;
				Stop_BGM();
				
				Start_BGM();
			}
		}
		else
			AudioSource_BGM.volume = theValue;
		
	}
	
	public bool isPlaying_BGM()
	{
		return AudioSource_BGM.isPlaying;
	}
	
	public void Resume_BGM()
	{
		if( AudioSource_BGM.isPlaying == false )
			AudioSource_BGM.Play();
	}
	
	
}